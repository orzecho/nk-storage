const path = require('path');

exports.createPages = async ({ graphql, actions, reporter }) => {
    const { createPage } = actions;
    const result = await graphql(`
      query MyQuery {
        allFile {
            nodes {
                internal {
                    mediaType
                }
            relativePath,
            publicURL
        }
      }
    }
    `
    );
    if (result.errors) {
        reporter.panicOnBuild(`Error while running GraphQL query.`);
        return;
    }
    const template = path.resolve(`src/template.js`);
    result.data.allFile.nodes
        .filter(node => node.internal.mediaType === 'audio/mpeg')
        .forEach(node => {
        const path = node.relativePath.replace('[NanoKarrin] ', '').replace(/ /g, '_');
        createPage({
            path,
            component: template,
            context: { node }
        })
    })
};